import Vue from 'vue'
import MainLayout from './layout/modules/main-layout.vue';
import VueRouter from 'vue-router'

import AppRoutes from './layout/routes';

Vue.use(VueRouter)
let router = new VueRouter({
    mode: 'history',
    routes: AppRoutes,
    scrollBehavior: function ( to, from ,savedPosition) {
        if(to.hash){
            return {
                behavior: 'smooth',
                selector: to.hash,
            }
        }
    },
})

// router.beforeEach((to, from, next)=>{
// })
Vue.config.productionTip = false
document.title = 'Test Project'

new Vue({
  router,
  render: h => h(MainLayout),
}).$mount('#app')
