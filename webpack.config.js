const path = require('path');


var RP = new RegExp("(tasks/vue-v_1)$");
var dir_name = __dirname.replace(RP, '');

module.exports = [
    {
        context: dir_name + '/app/modules',
        entry: {
            'layout': './layout/module',

        },
        output: {
            publicPath: '/local/templates/redesign/frontend/vue-build/',
            filename: '[name].js',
            chunkFilename: '[name].js',
            path: path.resolve(__dirname, 'vue-build')
        },
        watch: true,
        resolve: {
            alias: {
                'vue$': 'vue/dist/vue.esm.js' // 'vue/dist/vue.common.js' for webpack 1
            }
        }
    }];
